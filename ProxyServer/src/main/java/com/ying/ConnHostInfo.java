package com.ying;

import java.net.Socket;

/**
 * 目标主机信息以及相关的状态
 * @author zhengzhichao
 *
 */
public class ConnHostInfo {
	/**
	 * target注册过来的hostName
	 */
	private String hostName;
	
	/**
	 * 当前target是否还存活
	 */
	private Boolean isOnline;
	/**
	 * target当前状态
	 * 0 未激活，等待链接
	 * 1 target已激活，ProxyServer开始监听1123，等待target上线
	 * 2 target已上线，等待client上线
	 * 3 连接创建成功
	 */
	private int targetState;
	
	/**
	 * client当前状态
	 * 0 未激活
	 * 1 client已发送激活申请，请求targetName存在，proxyServer开始监听1124，等待client上线
	 * 2 client已上线，等待target上线
	 * 3 连接创建成功
	 */
	private int clientState;
	
	/**
	 * 当客户端发送激活申请时，指定的激活端口
	 */
	private String targetActivePort;
	
	/**
	 * target的socket
	 */
	private Socket targetSocket;
	/**
	 * client的socket
	 */
	private Socket clientSocket;
	
	/**
	 * 上次心跳时间
	 */
	private long lastHeartbeatMill;
	
	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public int getTargetState() {
		return targetState;
	}

	public void setTargetState(int targetState) {
		this.targetState = targetState;
	}

	public int getClientState() {
		return clientState;
	}

	public void setClientState(int clientState) {
		this.clientState = clientState;
	}

	public String getTargetActivePort() {
		return targetActivePort;
	}

	public void setTargetActivePort(String targetActivePort) {
		this.targetActivePort = targetActivePort;
	}

	public Socket getTargetSocket() {
		return targetSocket;
	}

	public void setTargetSocket(Socket targetSocket) {
		this.targetSocket = targetSocket;
	}

	public Socket getClientSocket() {
		return clientSocket;
	}

	public void setClientSocket(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	public Boolean getIsOnline() {
		return isOnline;
	}

	public void setIsOnline(Boolean isOnline) {
		this.isOnline = isOnline;
	}

	public long getLastHeartbeatMill() {
		return lastHeartbeatMill;
	}

	public void setLastHeartbeatMill(long lastHeartbeatMill) {
		this.lastHeartbeatMill = lastHeartbeatMill;
	}
	
}
