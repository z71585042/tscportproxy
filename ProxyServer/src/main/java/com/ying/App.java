package com.ying;


/**
 * Hello world!
 * 
 */
public class App {
	public static void main(String[] args) {
		Thread r1 = new Thread(new ProxyServer());
		r1.start();
		try {
			r1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
