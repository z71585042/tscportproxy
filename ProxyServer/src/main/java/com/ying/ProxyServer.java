package com.ying;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Hashtable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ying.common.IoBirdge;

public class ProxyServer implements Runnable{
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	/**
	 * 默认的服务端口
	 */
	private final int SERVER_UDP_PORT = 31122;
	/**
	 * target端监听端口
	 */
	private int targetListenPort = 31123;
	/**
	 * client端监听端口
	 */
	private int clientListenPort = 31124;
	/**
	 * 默认缓冲区大小
	 */
	private final int BUFFER_LENGTH = 1024;
	/**
	 * 目标主机信息，以及相关状态
	 */
	private Hashtable<String, ConnHostInfo> connHostInfos = new Hashtable<String,ConnHostInfo>();
	/**
	 * 当前连接信息
	 * 为了简化代码，我们假设客户端只能在一次请求要么建立完成，要么建立失败，不存在多个客户端同时操作的情况
	 */
	private volatile String currentConnHostName;
	
	/**
	 * currentConnHostName锁，记录上次锁定时间，1分钟锁定时间
	 */
	private volatile long lastCurrentConnHostNameLock;
	
	/**
	 * udp服务
	 */
	private DatagramSocket datagramSocket;
	
	public ProxyServer(){
		this.currentConnHostName = "default";
		//创建udp监听
		try {
			this.datagramSocket = new DatagramSocket(SERVER_UDP_PORT);
			this.datagramSocket.setSoTimeout(0);//不超时
		} catch (SocketException e1) {
			LOG.error("[create udp listenner(port:{}) error:{}]",SERVER_UDP_PORT,e1.getMessage(),e1);
		}
		/**
		 * 开启新线程tcp，监听端口proxyCPort（1124），等待client上线
		 */
		try {
			new ServiceThread(this.clientListenPort).start();
		} catch (IOException e1) {
			LOG.error("[create tcp listenner(port:{}) error:{}]",this.clientListenPort,e1.getMessage(),e1);
		}
		/**
		 * 开启新的线程 tcp，监听端口proxyTPort（1123）,等待target上线
		 */
		try {
			new ServiceThread(this.targetListenPort).start();
		} catch (IOException e1) {
			LOG.error("[create tcp listenner(port:{}) error:{}]",this.targetListenPort,e1.getMessage(),e1);
		}
		
		/**
		 * 启动一个检测连接的线程，每隔5s检测一次
		 */
		new Thread(){
			public void run(){
				while(true){
					try {
						Thread.sleep(1000*5);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					for(ConnHostInfo connHostInfo : connHostInfos.values()){//检测所有连接
						synchronized (connHostInfo) {//不允许同时操作此对象
							LOG.debug("[check conn : {}]",connHostInfo.getHostName());
							if(connHostInfo.getClientSocket() != null && connHostInfo.getTargetSocket() != null
									&& connHostInfo.getClientSocket().isConnected() && connHostInfo.getTargetSocket().isConnected()){//测链接创建成功的连接，是否已经断开

								//发送心跳探测链接是否有效
								try {
									connHostInfo.getClientSocket().sendUrgentData(0xFF);
									connHostInfo.getTargetSocket().sendUrgentData(0xFF);
									LOG.debug("[conn {} is ok]",connHostInfo.getHostName());
								} catch (IOException e1) {
									LOG.error("[conn {} is failed]",connHostInfo.getHostName(),e1);
									//关闭socket
									try {
										connHostInfo.getClientSocket().close();
										connHostInfo.getTargetSocket().close();
									} catch (IOException e) {
										LOG.error(e.getMessage(),e);
									}
									
									//重置当前链接状态
									connHostInfo.setClientSocket(null);
									connHostInfo.setTargetSocket(null);
									connHostInfo.setTargetActivePort("0");
									connHostInfo.setClientState(0);
									connHostInfo.setTargetState(0);
								}
								
								
							}
							
							//检测target是否还存活！
							long currentTime = System.currentTimeMillis();
							if( ((currentTime-connHostInfo.getLastHeartbeatMill())/1000) > 25 ){//超过25秒没有收到心跳
								LOG.debug("[conn {} is not Online]",connHostInfo.getHostName());
								//说明已经不在线
								connHostInfo.setIsOnline(false);
								connHostInfo.setClientState(0);
								connHostInfo.setTargetState(0);
							}
						}
					}
				}
			}
		}.start();
	}
	
	@Override
	public void run() {
		while(true){
			//监听接口数据
			DatagramPacket receivePacket = new DatagramPacket(new byte[this.BUFFER_LENGTH], this.BUFFER_LENGTH);
			try {
				this.datagramSocket.receive(receivePacket);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String receiveMsg = new String(receivePacket.getData()).trim().replaceAll("\n", "").replaceAll("\r", "");
			LOG.debug("[receive message:{}]",receiveMsg);
			
			/**
			 * 分析请求数据格式，做出不同的响应，目前用：分割,第一位表示请求来源(source flag)，第二位表示请求动作(action)，第三为表示参数（逗号分割）
			 * 1.c:create:targetName,targetPort     client请求与targetName（test）建立链接，并请求代理targetPort（22）端口
			 * 2.t:heartbeat:name                   target端心跳，通知proxy已经就绪
			 * 3.c:list 							列出上线targetName
			 */
			String[] reqs = receiveMsg.split(":");
			if(reqs.length > 1){//参数符合要求
				if("c".equals(reqs[0])){//client端请求
					if("create".equals(reqs[1])){//创建链接请求
						String[] param = reqs[2].split(",");
						if(param.length != 2){//参数格式不正确
							LOG.error("[error params:{}]",reqs[2]);
							receivePacket.setData(("error params:"+reqs[2]).getBytes());
						}else{//参数格式正确
							String targetName = param[0];
							String targetPort = param[1];
							if(this.connHostInfos.containsKey(targetName)){//请求targetName已经注册过,通知client上线
								
								//currentConnHostName是否被锁定
								if("default".equals(this.currentConnHostName) 
										|| (System.currentTimeMillis() - lastCurrentConnHostNameLock)/1000 > 60
										|| targetName.equals(this.currentConnHostName)){
									//当前请求targetName
									this.currentConnHostName = targetName;
									this.lastCurrentConnHostNameLock = System.currentTimeMillis();
									
									ConnHostInfo connHostInfo = this.connHostInfos.get(targetName);
									
									synchronized (connHostInfo) {
										if(connHostInfo.getClientState() != 3){//当前client状态不是已连接
											//修改connHostInfo状态
											connHostInfo.setClientState(1);
											connHostInfo.setTargetActivePort(targetPort);
											
											//返回 0:proxyCPort （0:1124）格式数据，通知客户端服务端准备就绪，等待client上线
											receivePacket.setData(("0:"+this.clientListenPort).getBytes());
										}else{
											//通知client 当前targetName已经有client连接，需要等待其他client断开
											receivePacket.setData("1:a connection already exists，please wait a moment".getBytes());
										}
									
									}
								}else{
									//通知client当前hostName被锁定
									receivePacket.setData(("1:currentHostName was locked by "+this.currentConnHostName+",please wait a moment").getBytes());
								}
								
							
							}else{//请求的targetName并没有注册过，返回提示
								//返回 1:提示信息 格式数据，表示请求失败，以及失败原因
								receivePacket.setData("1:targetName is not register".getBytes());
							}
						}
					}else if("list".equals(reqs[1])){//查询注册客户端请求
						receivePacket.setData(this.listTarget().getBytes());
					}else{
						LOG.error("[error action:{}]",receiveMsg);
						receivePacket.setData(("error action:"+receiveMsg).getBytes());
					}
				}else if("t".equals(reqs[0])){//target端请求
					if("heartbeat".equals(reqs[1])){//心跳请求
						String targetName = reqs[2];
						if(this.connHostInfos.containsKey(targetName)){//主机已经注册过，查看一下是否有客户端请求激活
							ConnHostInfo connHostInfo = this.connHostInfos.get(targetName);
							LOG.debug("[targetname:{}]",targetName);
							synchronized (connHostInfo) {
								//重置心跳时间
								connHostInfo.setLastHeartbeatMill(System.currentTimeMillis());
								connHostInfo.setIsOnline(true);
								
								if(connHostInfo.getClientState() != 0 && connHostInfo.getTargetState() == 0){//客户端已经发送过激活申请，通知target上线
									
									//修改状态
									connHostInfo.setTargetState(1);
									
									//返回 proxyTPort:targetPort （0:1123:22），通知target主动连接proxy的1123端口，并通知target代理本机22端口
									LOG.debug("0:"+this.targetListenPort+":"+connHostInfo.getTargetActivePort());
									receivePacket.setData(("0:"+this.targetListenPort+":"+connHostInfo.getTargetActivePort()).getBytes());
								}else{//客户端未发送过激活申请，返回0告诉客户端请求成功
									LOG.debug("0");
									receivePacket.setData("0".getBytes());
								}
							}
						}else{//未注册过就注册一下，并初始化一下状态
							ConnHostInfo connHostInfo = new ConnHostInfo();
							connHostInfo.setHostName(targetName);
							connHostInfo.setTargetState(0);
							connHostInfo.setClientState(0);
							connHostInfo.setLastHeartbeatMill(System.currentTimeMillis());
							connHostInfo.setIsOnline(true);
							this.connHostInfos.put(targetName, connHostInfo);
							
							//返回0告诉客户端请求成功
							receivePacket.setData("0".getBytes());
						}
					}else{
						LOG.error("[error action:{}]",receiveMsg);
						receivePacket.setData(("error action:"+receiveMsg).getBytes());
					}
				}else{
					LOG.error("[error source flag:{}]",receiveMsg);
					receivePacket.setData(("error source flag:"+receiveMsg).getBytes());
				}
			}else{
				LOG.error("[error request:{}]",receiveMsg);
				receivePacket.setData(("error request:"+receiveMsg).getBytes());
			}
			
			//返回响应信息
			try {
				this.datagramSocket.send(receivePacket);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private String listTarget(){
		StringBuilder ret = new StringBuilder();
		for(ConnHostInfo temp : this.connHostInfos.values()){
			if(temp.getIsOnline()){
				ret.append("targetName:").append(temp.getHostName())
				.append(",isOnline:").append(temp.getIsOnline())
				.append("\n");
			}
		}
		return ret.toString();
	}
	
	/**
	 * 校验一下当前链接是否已经符合联机条件，如果符合，则创建两个socket的桥接
	 */
	/*public synchronized void checkConnSuccess(){
		LOG.debug("[checking conn]");
		ConnHostInfo connHostInfo = this.connHostInfos.get(currentConnHostName);
		if(connHostInfo.getClientState() == 2 && connHostInfo.getTargetState() == 2){//客户端，服务端均已经就位
			Socket clientSocket = connHostInfo.getClientSocket();
			Socket targetSocket = connHostInfo.getTargetSocket();
			if(connHostInfo.getTargetSocket().isConnected() && connHostInfo.getClientSocket().isConnected()){//链接均有效
				*//**
				 * 桥接两个socket的输入输出
				 *//*
				try {
					new IoBirdge(clientSocket.getInputStream(), targetSocket.getOutputStream()).start();
					new IoBirdge(targetSocket.getInputStream(), clientSocket.getOutputStream()).start();
					LOG.debug("[birdge conn success!]");
					
					//修改状态
					connHostInfo.setClientState(3);
					connHostInfo.setTargetState(3);
					
					//修改当前链接名字
					this.currentConnHostName = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}else{
				LOG.error("[target or client tcp connection is closed]");
			}
		}else{
			LOG.debug("[not all on line. clientState:{},targetState:{}]",connHostInfo.getClientState(),connHostInfo.getTargetState());
		}
	}*/
	
	/**
	 * 生成端口服务的线程
	 * @author zhengzhichao
	 *
	 */
	class ServiceThread extends Thread{
		/**
		 * 监听端口
		 */
		private int port;
		
		private ServerSocket serverSocket;
		
		public ServiceThread(int port) throws IOException{
			this.port = port;
			
			//开启监听
			this.serverSocket = new ServerSocket(this.port);
			this.serverSocket.setSoTimeout(0);
		}
		
		@Override
		public void run(){
			while(true){
				//开启监听
				LOG.debug("[tcp listen in {} port success]",this.port);
				Socket socket = null;
				try {
					socket = serverSocket.accept();
					InetAddress inteAddress = socket.getInetAddress(); 
					LOG.debug("[SocketIp:{}][SocketPort:{}][connected]",inteAddress.getHostAddress(),socket.getPort());
				
					if(!"default".equals(currentConnHostName)){
						//获取当前链接信息，记录,更新状态
						ConnHostInfo connHostInfo = connHostInfos.get(currentConnHostName);
						
						Thread t1 = null;
						Thread t2 = null;
						synchronized (connHostInfo) {//同步锁，不允许同时操作此对象
							if(this.port == targetListenPort){//target端口
								connHostInfo.setTargetState(2);
								connHostInfo.setTargetSocket(socket);
							}else if(this.port == clientListenPort){//client端口
								connHostInfo.setClientState(2);
								connHostInfo.setClientSocket(socket);
							}else{
								LOG.error("[error port:{}]",this.port);
							}
							
							//检查联机条件，桥接
							if(connHostInfo.getClientState() == 2 && connHostInfo.getTargetState() == 2){//客户端，服务端均已经就位
								Socket clientSocket = connHostInfo.getClientSocket();
								Socket targetSocket = connHostInfo.getTargetSocket();
								
								try{
									clientSocket.sendUrgentData(0xFF);
									targetSocket.sendUrgentData(0xFF);
									/**
									 * 桥接两个socket的输入输出
									 */
									t1 = new IoBirdge(clientSocket.getInputStream(), targetSocket.getOutputStream());
									t2 = new IoBirdge(targetSocket.getInputStream(), clientSocket.getOutputStream());
									LOG.debug("[birdge conn success!]");
									
									//修改状态
									connHostInfo.setClientState(3);
									connHostInfo.setTargetState(3);
									
									//联机成功，修改当前链接名字
									currentConnHostName = "default";
									
									t1.start();
									t2.start();
								}catch(IOException e1){
									LOG.error("[target or client tcp connection is closed]",e1);
								}
								
							}else{
								LOG.debug("[not all on line. clientState:{},targetState:{}]",connHostInfo.getClientState(),connHostInfo.getTargetState());
							}
						}
						if(t1 != null && t2 != null){
							try {
								t1.join();
								t2.join();
							} catch (InterruptedException e) {
								LOG.error(e.getMessage(),e);
							}
						}
					}else{
						LOG.error("[current conn is null]");
						socket.close();
					}
				} catch (IOException e1) {
					LOG.error("[ServiceThread (port:{}) error:{}]",this.port,e1.getMessage(),e1);
				}
			}
		}
	}
	
}
