package com.ying.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 输入输出桥接类
 * @author zhengzhichao
 *
 */
public class IoBirdge extends Thread {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	private InputStream inputStream;
	private OutputStream outputStream;
	
	public IoBirdge(InputStream inputStream,OutputStream outputStream){
		this.inputStream = inputStream;
		this.outputStream = outputStream;
	}
	
	@Override
	public void run() {
		try {
			int n;
			while((n = this.inputStream.read()) != -1){
//				LOG.debug("read String:{}",n);
				this.outputStream.write(n);
				this.outputStream.flush();
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(),e);
		}
	}
}
