package com.ying;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Test3389Port {
	public static void main(String[] args){
		try {
			Socket targetSocket = new Socket("192.168.163.49",3389);
			targetSocket.setSoTimeout(0);
			int n;
			while((n = targetSocket.getInputStream().read()) != -1){
				System.out.println(n);
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
