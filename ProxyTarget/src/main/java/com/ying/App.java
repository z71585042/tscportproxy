package com.ying;

/**
 * Hello world!
 * 
 */
public class App {
	public static void main(String[] args) {
		TargetServer targetServer = new TargetServer("test");
		Thread t = new Thread(targetServer);
		t.start();
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
