package com.ying;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ying.common.IoBirdge;

/**
 * 目标服务
 * @author zhengzhichao
 *
 */
public class TargetServer implements Runnable {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 默认缓冲区大小
	 */
	private final int BUFFER_LENGTH = 1024;
	/**
	 * 远程ip
	 */
	private final String REMOTE_SERVER_IP = "192.168.163.48";
	/**
	 * 远程udp端口
	 */
	private final int REMOTE_SERVER_UDP_PORT = 31122;
	
	/**
	 * 是否已经在线，默认为不在线
	 */
	private volatile Boolean isOnline;
	
	/**
	 * targetName
	 */
	private String targetName;
	/**
	 * 心跳请求
	 */
	private String heartbeatReq;
	
	/**
	 * 激活端口socket
	 */
	private Socket targetSocket;
	/**
	 * 对外连接socket
	 */
	private Socket outSocket;
	
	public TargetServer(String targetName){
		this.targetName = targetName;
		
		this.heartbeatReq = "t:heartbeat:"+this.targetName;
		this.isOnline = false;
		
		//启动一个监控线程，每隔一段时间检测以下socket是否已关闭
		new Thread(){
			public void run(){
				while(true){
					try {
						Thread.sleep(1000*5);//每5秒钟检测一次
						
						//当前链接存在
						if(targetSocket != null && outSocket != null
								&& targetSocket.isConnected() && outSocket.isConnected()){
							try {
								LOG.debug("[checking conn ....]");
//								targetSocket.sendUrgentData(0xFF);
								outSocket.sendUrgentData(0xFF);
							} catch (IOException e1) {
								LOG.error(e1.getMessage(),e1);
								LOG.debug("[one socket conn is closed,close another one,reset target]");
								try {
									targetSocket.close();
									outSocket.close();
									targetSocket = null;
									outSocket = null;
								} catch (IOException e) {
									LOG.error(e.getMessage(),e);
								}
								//修改online标志位
								isOnline = false;
							}
							/*if(targetSocket.isClosed() || outSocket.isClosed()){//只要其中一个关闭，就全都关闭
								try {
									LOG.debug("[one socket conn is closed,close another one,reset target]");
									targetSocket.close();
									outSocket.close();
								} catch (IOException e) {
									e.printStackTrace();
								}
								
								//修改online标志位
								isOnline = false;
							}*/
							
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}
	
	/**
	 * 定时发送心跳到服务器
	 */
	@Override
	public void run() {
		while(true){
			DatagramSocket datagramSocket = null;
			try {
				Thread.sleep(1000*10);//10s心跳一次
				/**
				 * udp心跳请求
				 */
				datagramSocket = new DatagramSocket();
				datagramSocket.setSoTimeout(1000*5);
				//请求内容
				byte[] req = this.heartbeatReq.getBytes();
				DatagramPacket packet = new DatagramPacket(req,req.length,InetAddress.getByName(REMOTE_SERVER_IP),REMOTE_SERVER_UDP_PORT);
				//发送
				datagramSocket.send(packet);
				//接受
				DatagramPacket receivePacket = new DatagramPacket(new byte[this.BUFFER_LENGTH], this.BUFFER_LENGTH);
				datagramSocket.receive(receivePacket);
				String msg = new String(receivePacket.getData()).trim().replace("\n", "").replace("\r", "");
				if("0".equals(msg)){//请求成功
					//do nothing
				}else{//客户端请求激活
					
					if(!isOnline){//当前target不在线
						String[] param = msg.split(":");
						int activePort = Integer.valueOf(param[2]);
						int serverPort = Integer.valueOf(param[1]);
						
						//开启目标端口连接
						targetSocket = new Socket("127.0.0.1",activePort);
						targetSocket.setSoTimeout(0);
						//开启远程端口连接
						outSocket = new Socket(REMOTE_SERVER_IP,serverPort);
						outSocket.setSoTimeout(0);
						
						if(this.targetSocket != null && this.outSocket != null
								&& this.targetSocket.isConnected() && this.outSocket.isConnected()){
							
							new IoBirdge(targetSocket.getInputStream(), outSocket.getOutputStream()).start();
							new IoBirdge(outSocket.getInputStream(), targetSocket.getOutputStream()).start();
							LOG.debug("[target birdge success]");
							
							isOnline = true;
						}else{
							LOG.debug("target birdge failed");
							isOnline = false;
						}
					}
					
				}
				
				
			} catch (SocketException e) {
				e.printStackTrace();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}finally{
				if(datagramSocket != null){
					datagramSocket.close();
				}
			}
		}
	}
}
