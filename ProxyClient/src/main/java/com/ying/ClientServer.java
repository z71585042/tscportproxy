package com.ying;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ying.common.IoBirdge;

/**
 * 
 * 客户端服务
 * 连接服务端端口并启动本地监听，将服务端数据转发到本地端口
 * @author zhengzhichao
 *
 */
public class ClientServer implements Runnable {
	private final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 连接服务端的socket
	 */
	private Socket socket;
	/**
	 * 本地监听端口
	 */
	private int localListenPort;
	
	/**
	 * 是否继续监听
	 */
	private volatile Boolean listenFlag;
	
	public ClientServer(Socket socket,int localListenPort){
		this.socket = socket;
		this.localListenPort = localListenPort;
		this.listenFlag = true;
	}
	
	@Override
	public void run() {
		//开启本地端口
		//开启监听
		LOG.debug("[client is open listen in port {}]",this.localListenPort);
		try {
			final ServerSocket serverSocket = new ServerSocket(this.localListenPort);
		
			//启动一个监控线程，每隔一段时间检测一下远程socket是否已关闭
			new Thread(){
				public void run(){
					while(true){
						LOG.debug("[checking conn ....]");
						try {
							Thread.sleep(1000*5);//每5秒钟检测一次
							
							try {
								socket.sendUrgentData(0xFF);//心跳检测
							} catch (IOException e1) {
								LOG.debug("[remote socket conn is closed,close local listenner]");
								//修改标志位,退出监听
								listenFlag = false;
								try {
									serverSocket.close();//关闭本地监听
								} catch (IOException e) {
									e.printStackTrace();
								}
								break;//退出监控线程
							}
							
							
							/*if(socket.isClosed()){//远程socket已关闭
								try {
									LOG.debug("[remote socket conn is closed,close local listenner]");
									//修改标志位,退出监听
									listenFlag = false;
									serverSocket.close();
								} catch (IOException e) {
									e.printStackTrace();
								}
								
							}*/
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}.start();
			
			while(listenFlag){
				Socket clientSocket;
				try {
					clientSocket = serverSocket.accept();
					//桥接两个的输入输出
					new IoBirdge(clientSocket.getInputStream(), this.socket.getOutputStream()).start();
					new IoBirdge(this.socket.getInputStream(), clientSocket.getOutputStream()).start();
				} catch (IOException e1) {
					LOG.error(e1.getMessage(),e1);
				}
				
			}
			
			LOG.debug("client conn is exit");
		} catch (IOException e1) {
			LOG.error(e1.getMessage(),e1);
		}
		
		System.out.println("[client exit]");
	
	}
	
}
