package com.ying;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

/**
 * Hello world!
 * 
 */
public class App {
	private static final String REMOTE_SERVER_IP = "127.0.0.1";
	private static final int REMOTE_SERVER_UDP_PORT = 31122;
	private static final int REMOTE_SERVER_TCP_PORT = 31124;
	/**
	 * 默认缓冲区大小
	 */
	private static final int BUFFER_LENGTH = 1024;
	
	public static void main(String[] args) {
		
		//构建udp请求
		DatagramSocket datagramSocket = null;
		try {
			datagramSocket = new DatagramSocket();
			datagramSocket.setSoTimeout(1000*5);
		
			//等待指令
			System.out.println("eg:create:targetName,remotePort,localPort");
			System.out.println("eg:list");
			BufferedReader strin = new BufferedReader(new InputStreamReader(System.in));
			while(true){
				String temp;
				try {
					System.out.print("client>");
					temp = strin.readLine();
					if(!"".equals(temp)){
						if(temp.startsWith("create:")){//创建端口映射命令
							String[] param = temp.substring(temp.indexOf(":")+1, temp.length()).split(",");
							if(param.length == 3){//发送激活请求
								//请求内容
								byte[] req = ("c:create:"+param[0]+","+param[1]).getBytes();
								DatagramPacket packet = new DatagramPacket(req,req.length,InetAddress.getByName(REMOTE_SERVER_IP),REMOTE_SERVER_UDP_PORT);
								//发送
								datagramSocket.send(packet);
								
								//接受
								DatagramPacket receivePacket = new DatagramPacket(new byte[BUFFER_LENGTH], BUFFER_LENGTH);
								datagramSocket.receive(receivePacket);
								String msg = new String(receivePacket.getData()).trim().replace("\n", "").replace("\r", "");
								
								if(msg.startsWith("0")){//请求创建成功
									//客户端准备上线
									//开启poxyserver端口连接
									Socket socket = new Socket(REMOTE_SERVER_IP,REMOTE_SERVER_TCP_PORT);
									
									//启动监听
									new Thread(new ClientServer(socket, Integer.valueOf(param[2]))).start();
									System.out.println("conn create success!");
								}else{//请求处理失败
									System.out.println("server response:"+msg);
								}
							}else{
								System.out.println("error:please input targetName and portNum split by ,");
							}
						}else if("list".equals(temp)){//查询命令
							//请求内容
							byte[] req = ("c:list").getBytes();
							DatagramPacket packet = new DatagramPacket(req,req.length,InetAddress.getByName(REMOTE_SERVER_IP),REMOTE_SERVER_UDP_PORT);
							//发送
							datagramSocket.send(packet);
							
							//接受
							DatagramPacket receivePacket = new DatagramPacket(new byte[BUFFER_LENGTH], BUFFER_LENGTH);
							datagramSocket.receive(receivePacket);
							String msg = new String(receivePacket.getData()).trim();
							
							System.out.println(msg);
						}else{
							System.out.println("error:can't be identified command");
						}
					}else{
						System.out.println("error:can't be empty");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
	}
}
