package com.ying;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class TestSyn {
	private Wrap wrap;
	
	public static void main(String[] args){
		TestSyn testSyn = new TestSyn();
		testSyn.test();
	}
	
	public void test(){
		try {
			Socket socket = new Socket("127.0.0.1", 2837);
			
			wrap = new Wrap(socket);
			
			
			Thread t1 = new Thread(){
				public void run(){
					try {
						Thread.sleep(1000*1);
						synchronized(wrap){
							
							new Thread(){
								public void run(){
									while(true){
										try {
											Thread.sleep(1000*1);
											BufferedOutputStream bufferedOutputStream =  new BufferedOutputStream(wrap.getSocket().getOutputStream());
											OutputStreamWriter outputStreamWriter = new OutputStreamWriter(bufferedOutputStream);
											outputStreamWriter.write("gagagaga");
											outputStreamWriter.flush();
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}catch (InterruptedException e) {
											e.printStackTrace();
										}
									}
								}
							}.start();
						}
//					} catch (IOException e) {
//						e.printStackTrace();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
			t1.start();
			
			Thread t2 = new Thread(){
				public void run(){
					while(true){
						synchronized (wrap) {
							try {
								System.out.println("syn begin!");
								Thread.sleep(1000*10);
								System.out.println("syn end!");
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						try {
							Thread.sleep(1000*5);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			};
			t2.start();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	class Wrap{
		private Socket socket;
		public Wrap(Socket socket){
			this.socket = socket;
		}
		public Socket getSocket() {
			return socket;
		}
		public void setSocket(Socket socket) {
			this.socket = socket;
		}
	}
}
